const express = require('express');

const app = express();

app.use((req, res, next) => {
  if (req.url.endsWith('.br')) {
    res.setHeader('content-encoding', 'br');
  }

  if (req.url.includes('wasm')) {
    res.setHeader('content-type', 'application/wasm');
  }

  next();
});
app.use(express.static('./gang'));

app.listen(8080);
